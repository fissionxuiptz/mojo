# frozen_string_literal: true

require "test_helper"

module CoreExt
  class KernelTest < Mojo::Test
    test "#ruby_version" do
      assert_respond_to Kernel, :ruby_version
      assert_match(/#{RUBY_VERSION}$/, ruby_version)
    end
  end
end
