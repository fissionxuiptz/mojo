# frozen_string_literal: true

require "test_helper"

module CoreExt
  module Object
    class TapIfTest < Mojo::Test
      def assert_tapped
        @mock.expect :call, true

        yield

        assert_mock @mock
        assert @result
      end

      def assert_untapped
        yield

        assert_mock @mock
        assert_nil @result
      end

      def assert_returned(obj)
        @mock.expect :call, true

        assert_equal obj.object_id, yield(obj).object_id
      end

      attr_reader :block

      setup do
        @mock = Minitest::Mock.new

        @result = nil

        @block = ->(obj) {
          @mock.call
          @result = obj
        }
      end

      test "is a method on all objects" do
        assert_respond_to ::Object.new, :tap_if
        assert_respond_to ::Object, :tap_if
        assert_respond_to nil, :tap_if
      end

      context "when the receiver is truthy" do
        test "executes the block" do
          assert_tapped { true.tap_if(&block) }
          assert_tapped { "".tap_if(&block) }
          assert_tapped { 1.tap_if(&block) }
          assert_tapped { [].tap_if(&block) }
          assert_tapped { {}.tap_if(&block) }
          assert_tapped { 0.tap_if(&block) }
          assert_tapped { :foo.tap_if(&block) }
        end

        test "returns the receiver" do
          assert_returned(true) { |obj| obj.tap_if(&block) }
          assert_returned("") { |obj| obj.tap_if(&block) }
          assert_returned(1) { |obj| obj.tap_if(&block) }
          assert_returned([]) { |obj| obj.tap_if(&block) }
          assert_returned({}) { |obj| obj.tap_if(&block) }
          assert_returned(0) { |obj| obj.tap_if(&block) }
          assert_returned(:foo) { |obj| obj.tap_if(&block) }
        end
      end

      context "when the receiver is falsey" do
        test "does not execute the block" do
          assert_untapped { false.tap_if(&block) }
          assert_untapped { nil.tap_if(&block) }
        end

        test "returns the receiver" do
          assert_returned(false) { |obj| obj.tap_if(&block) }
        end
      end

      context "when the method argument evaluates truthily" do
        test "executes the block" do
          assert_tapped { 1.tap_if(:positive?, &block) }
          assert_tapped { [].tap_if(:empty?, &block) }
          assert_tapped { { foo: :bar }.tap_if(:key?, :foo, &block) }
        end

        test "returns the receiver" do
          assert_returned(1) { |obj| obj.tap_if(:positive?, &block) }
          assert_returned([]) { |obj| obj.tap_if(:empty?, &block) }
          assert_returned(foo: :bar) { |obj| obj.tap_if(:key?, :foo, &block) }
        end
      end

      context "when the method argument evaluates falsily" do
        test "does not execute the block" do
          assert_untapped { 1.tap_if(:negative?, &block) }
          assert_untapped { [].tap_if(:any?, &block) }
          assert_untapped { { foo: :bar }.tap_if(:key?, :baz, &block) }
        end

        test "returns the receiver" do
          assert_returned(1) { |obj| obj.tap_if(:negative?, &block) }
          assert_returned([]) { |obj| obj.tap_if(:any?, &block) }
          assert_returned(foo: :bar) { |obj| obj.tap_if(:key?, :baz, &block) }
        end
      end

      context "when the method argument is a lambda" do
        test "executes the block if it evaluates truthily" do
          assert_tapped { "foo".tap_if(-> { length == 3 }, &block) }
        end

        test "does not execute the block if it evaluates falsily" do
          assert_untapped { "foo".tap_if(-> { length.zero? }, &block) }
        end
      end

      context "when the method is not defined" do
        test "does not execute the block" do
          assert_untapped { 1.tap_if(:foobar) }
          assert_untapped { [].tap_if(:foobar) }
        end
      end
    end
  end
end
