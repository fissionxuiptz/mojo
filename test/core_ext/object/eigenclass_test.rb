# frozen_string_literal: true

require "test_helper"

module CoreExt
  module Object
    class EigenclassTest < ActiveSupport::TestCase
      test "is a method on all objects" do
        assert_respond_to ::Object.new, :eigenclass
        assert_respond_to ::Object, :eigenclass
        assert_respond_to Module, :eigenclass
      end

      test "returns the singleton class of an object" do
        [::Object, ::Object.new, Module, "foo"].each do |obj|
          assert_raises(TypeError, "can't create instance of singleton class") do
            obj.eigenclass.new
          end
        end
      end
    end
  end
end
