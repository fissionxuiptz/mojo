# frozen_string_literal: true

require "test_helper"

module CoreExt
  module Object
    class TapUnlessTest < Mojo::Test
      def assert_tapped(ignore_result: false)
        @mock.expect :call, true

        yield

        assert_mock @mock
        assert @result unless ignore_result
      end

      def assert_untapped
        yield

        assert_mock @mock
        assert_nil @result
      end

      def assert_returned(obj)
        @mock.expect :call, true

        assert_equal obj.object_id, yield(obj).object_id
      end

      attr_reader :block

      setup do
        @mock = Minitest::Mock.new

        @result = nil

        @block = ->(obj) {
          @mock.call
          @result = obj
        }
      end

      test "is a method on all objects" do
        assert_respond_to ::Object.new, :tap_unless
        assert_respond_to ::Object, :tap_unless
        assert_respond_to nil, :tap_unless
      end

      context "when the receiver is truthy" do
        test "does not execute the block" do
          assert_untapped { true.tap_unless(&block) }
          assert_untapped { "".tap_unless(&block) }
          assert_untapped { 1.tap_unless(&block) }
          assert_untapped { [].tap_unless(&block) }
          assert_untapped { {}.tap_unless(&block) }
          assert_untapped { 0.tap_unless(&block) }
          assert_untapped { :foo.tap_unless(&block) }
        end

        test "returns the receiver" do
          assert_returned(true) { |obj| obj.tap_unless(&block) }
          assert_returned("") { |obj| obj.tap_unless(&block) }
          assert_returned(1) { |obj| obj.tap_unless(&block) }
          assert_returned([]) { |obj| obj.tap_unless(&block) }
          assert_returned({}) { |obj| obj.tap_unless(&block) }
          assert_returned(0) { |obj| obj.tap_unless(&block) }
          assert_returned(:foo) { |obj| obj.tap_unless(&block) }
        end
      end

      context "when the receiver is falsey" do
        test "executes the block" do
          assert_tapped(ignore_result: true) { false.tap_unless(&block) }
          assert_tapped(ignore_result: true) { nil.tap_unless(&block) }
        end

        test "returns the receiver" do
          assert_returned(false) { |obj| obj.tap_unless(&block) }
        end
      end

      context "when the method argument evaluates truthily" do
        test "does not execute the block" do
          assert_untapped { 1.tap_unless(:positive?, &block) }
          assert_untapped { [].tap_unless(:empty?, &block) }
          assert_untapped { { foo: :bar }.tap_unless(:key?, :foo, &block) }
        end

        test "returns the value" do
          assert_returned(1) { |obj| obj.tap_unless(:positive?, &block) }
          assert_returned([]) { |obj| obj.tap_unless(:empty?, &block) }
          assert_returned(foo: :bar) { |obj| obj.tap_unless(:key?, :foo, &block) }
        end
      end

      context "when the method argument evaluates falsily" do
        test "executes the block" do
          assert_tapped { 1.tap_unless(:negative?, &block) }
          assert_tapped { [].tap_unless(:any?, &block) }
          assert_tapped { { foo: :bar }.tap_unless(:key?, :baz, &block) }
          assert_tapped { :foobar.tap_unless(nil, &block) }
        end

        test "returns the receiver" do
          assert_returned(1) { |obj| obj.tap_unless(:negative?, &block) }
          assert_returned([]) { |obj| obj.tap_unless(:any?, &block) }
          assert_returned(foo: :bar) { |obj| obj.tap_unless(:key?, :baz, &block) }
          assert_returned(:foobar) { |obj| obj.tap_unless(nil, &block) }
        end
      end

      context "when the method argument is a lambda" do
        test "executes the block if it evaluates falsily" do
          assert_tapped { "foo".tap_unless(-> { length.zero? }, &block) }
        end

        test "does not execute the block if it evaluates truthily" do
          assert_untapped { "foo".tap_unless(-> { length == 3 }, &block) }
        end
      end

      context "when the method is not defined" do
        test "executes the block" do
          assert_tapped { 1.tap_unless(:foobar, &block) }
          assert_tapped { [].tap_unless(:foobar, &block) }
        end
      end

      context "when the receiver is nil" do
        test "does not execute the block when the argument is :nil?" do
          assert_untapped { nil.tap_unless(:nil?, &block) }
        end
      end
    end
  end
end
