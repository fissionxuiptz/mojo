# frozen_string_literal: true

require "test_helper"

module CoreExt
  module Object
    class InterestingMethodsTest < ActiveSupport::TestCase
      test "is a method on all objects" do
        [:interesting_methods, :im].each do |method|
          assert_respond_to ::Object.new, method
          assert_respond_to ::Object, method
          assert_respond_to Module, method
        end
      end

      test "returns sorted methods from a class" do
        expected = %i[bar_class_method class_var_reader]

        assert_equal expected, Bar.im
      end

      test "returns sorted methods from an instance" do
        expected = %i[bar_instance_method bar_other_instance_method]

        assert_equal expected, Bar.new("bar").im
      end

      test "returns sorted methods from an inherited class" do
        expected = %i[bar_class_method class_var_reader foo_class_method]

        assert_equal expected, Foo.im
      end

      test "returns sorted methods from an inherited instance" do
        expected = %i[bar_instance_method bar_other_instance_method foo_instance_method]

        assert_equal expected, Foo.new("foo").im
      end

      test "returns sorted methods from a module" do
        expected = %i[qux_class_method qux_instance_method]

        assert_equal expected, Qux.im
      end

      test "returns sorted methods from an extended class" do
        expected = %i[bar_class_method class_var_reader foo_class_method qux_instance_method]

        assert_equal expected, QuxExtended.im
      end

      test "returns sorted methods from an included class" do
        expected = %i[
          bar_instance_method bar_other_instance_method foo_instance_method qux_instance_method
        ]

        assert_equal expected, QuxIncluded.new("foobar").im
      end
    end
  end
end
