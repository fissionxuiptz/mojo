# frozen_string_literal: true

require "simplecov"

SimpleCov.start do
  enable_coverage :branch
  add_filter %r{^/test/}
  add_filter { |file| file.lines.count < 5 }
end

require "pry"
require "minitest/autorun"
require "minitest/reporters"
require "minitest/benchmark"
require "mojo/console"
require "active_support/test_case"

Minitest::Reporters.use! Minitest::Reporters::ProgressReporter.new

Dir[File.expand_path("support/**/*.rb", __dir__)].sort.each(&method(:require))

module Mojo
  class Test < ActiveSupport::TestCase
    def self.context(desc, &block)
      desc = desc.titleize.gsub(/\W/, "")
      klass = const_set(desc, Class.new(self))

      klass.class_eval do
        instance_methods.each do |method|
          undef_method(method) if method =~ /^test_/
        end
      end

      klass.class_eval(&block)
    end
  end
end
