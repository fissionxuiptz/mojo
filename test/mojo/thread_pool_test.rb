# frozen_string_literal: true

require "test_helper"

module Mojo
  class ThreadPoolTest < Test
    def time_taken
      now = Time.now.to_f
      yield
      Time.now.to_f - now
    end

    setup do
      @pool_size = 5
      @pool = ThreadPool.new(@pool_size)
      @mutex = Mutex.new
    end

    test "basic usage" do
      iterations = @pool_size * 3
      results = Array.new(iterations)

      iterations.times do |i|
        @pool.call do
          @mutex.synchronize do
            results[i] = i + 1
          end
        end
      end

      @pool.shutdown

      assert_equal 1.upto(@pool_size * 3).to_a, results
    end

    test "time savings" do
      elapsed = time_taken do
        @pool_size.times do
          @pool.call { sleep 0.1 }
        end

        @pool.shutdown
      end

      assert_operator 0.2, :>, elapsed, format("Elapsed time was too long: %.1f seconds", elapsed)
    end

    test "pool size limits" do
      threads = Set.new

      1_000.times do
        @pool.call do
          @mutex.synchronize do
            sleep 0.000000001
            threads << Thread.current
          end
        end
      end

      @pool.shutdown

      assert_equal @pool_size, threads.size
    end

    test "thread ids" do
      @id = nil

      @pool.call do
        @id = Thread.current[:id]
      end

      @pool.shutdown

      assert_equal @id, "#{@pool.object_id}:#{@pool_size}:0"
    end
  end
end
