# frozen_string_literal: true

require "test_helper"
require "pry/testable"

using Mojo::Shouter::Types

module Mojo
  class ShouterTest < Test
    define_method(:mu_pp, &:itself)

    Shouter::Shout.prepend(
      Module.new do
        private

        def window_width
          200
        end
      end
    )

    test "block takes precedence over argument" do
      assert_output shout_content("true", "TrueClass < Object") do
        false.shout { true }
      end
    end

    test "long strings" do
      width = Shouter::Shout.new(nil).instance_exec { window_width }

      assert_output(
        /
          "c{#{width - 7}}[^c]+
          [^c]+c{#{width - 6}}[^c]+
          [^c]+c{#{width - 7}}"
        /mx
      ) { ("c" * ((width * 3) - 20)).shout }
    end

    test "returns the object" do
      result = true
      capture_io { result = :foo.shout }

      assert_equal :foo, result
    end

    test "returns nil when called from pry" do
      result = true
      capture_io { result = Pry::Testable::PryTester.new.eval(":foo.shout") }

      assert_equal DevNull, result
    end

    context "formatting" do
      context "simple types" do
        test "finite BigDecimal" do
          assert_shout("6.0", title: "BigDecimal < Numeric") { 6.0.to_d }
        end

        test "infinite BigDecimal" do
          assert_shout("Infinity") { 6.0.to_d / 0 }
        end

        test "the rest" do
          assert_shout("nil", title: "NilClass < Object") { nil }
          assert_shout("true", title: "TrueClass < Object") { true }
          assert_shout("false", title: "FalseClass < Object") { false }
          assert_shout(":foo", title: "Symbol < Object") { :foo }
          assert_shout('"foo"', title: "String < Object") { "foo" }
          assert_shout("Mojo", title: "Module < Object") { Mojo }
        end
      end

      context "Regexps" do
        test "simple Regexp" do
          assert_shout("/foo/m", title: "Regexp < Object") do
            /foo/m
          end
        end

        test "complex Regexp" do
          assert_shout(
            <<~CONTENT
              /
                (?:              # foo
                  [^\\(\\)]*       # bar
                  (\\([^\\n]*?\\))  # baz
                )*               # qux
                [^\\(\\)]*         # quux
              /x
            CONTENT
          ) do
            Regexp.new(
              [
                "",
                "  (?:              # foo",
                '    [^\(\)]*       # bar',
                '    (\([^\n]*?\))  # baz',
                "  )*               # qux",
                '  [^\(\)]*         # quux',
                ""
              ].join("\n"),
              Regexp::EXTENDED
            )
          end
        end
      end

      context "Classes" do
        test "BasicObject" do
          assert_shout("BasicObject", title: "Class < Module") do
            BasicObject
          end
        end

        test "core class" do
          assert_shout("String < Object") { String }
        end

        test "core subclass" do
          assert_shout("Foo < Bar") { Foo }
        end

        test "delegate class" do
          assert_shout("Baz < DelegateClass(Bar)", title: "Class < Module") { Baz }
        end

        test "delegate class instance" do
          assert_shout('{ "attr_reader" => "foo" }', title: "Baz < DelegateClass(Bar)") do
            Baz.new(
              Bar.new(:foo)
            )
          end
        end

        test "delegated subclass instance" do
          expected = <<~CONTENT
            {
              "table" => {
                "foo" => "bar",
                "bar" => "baz",
                "baz" => [1, 2, "qux"]
              }
            }
          CONTENT

          assert_shout(expected, title: "Baz < DelegateClass(OpenStruct)") do
            Baz.new(
              OpenStruct.new(foo: :bar, bar: :baz, baz: [1, 2, :qux]) # rubocop:disable Style/OpenStructUse
            )
          end
        end
      end

      context "Arrays" do
        test "empty" do
          assert_shout("[]", title: "Array < Object") { [] }
        end

        test "simple with symbols" do
          assert_shout("[:foo, :bar]") { %i[foo bar] }
        end

        test "simple without symbols" do
          assert_shout('["foo", "bar", "baz"]') { %w[foo bar baz] }
        end

        test "complex data types" do
          assert_shout(
            <<~CONTENT
              [
                {
                  foo: {
                    bar: :baz
                  }
                }
              ]
            CONTENT
          ) { [{ foo: { bar: :baz } }] }
        end
      end

      context "Hashes" do
        test "empty" do
          assert_shout "{}", title: "Hash < Object" do
            {}
          end
        end

        test "simple with symbols" do
          assert_shout "{ foo: :bar }" do
            { foo: :bar }
          end
        end

        test "simple without symbols" do
          assert_shout '{ "foo" => "bar" }' do
            { "foo" => "bar" }
          end
        end

        test "2 keys" do
          assert_shout "{ foo: :bar, bar: :baz }" do
            { foo: :bar, bar: :baz }
          end
        end

        test "3 keys" do
          assert_shout(
            <<~CONTENT
              {
                foo: :bar,
                bar: :baz,
                baz: :qux
              }
            CONTENT
          ) do
            { foo: :bar, bar: :baz, baz: :qux }
          end
        end

        test "complex data types" do
          assert_shout(
            <<~CONTENT
              {
                foo: {
                  bar: {
                    baz: [1, 2, 3]
                  }
                }
              }
            CONTENT
          ) do
            { foo: { bar: { baz: [1, 2, 3] } } }
          end

          assert_shout(
            <<~CONTENT
              {
                foo: {
                  bar: {
                    baz: [
                      {
                        "attr_reader" => "qux"
                      }
                    ]
                  }
                }
              }
            CONTENT
          ) do
            { foo: { bar: { baz: [Foo.new(:qux)] } } }
          end
        end
      end
    end
  end
end
