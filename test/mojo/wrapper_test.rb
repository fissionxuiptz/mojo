# frozen_string_literal: true

require "test_helper"

module Mojo
  class WrapperTest < Test
    RESULT = [] # rubocop:disable Style/MutableConstant

    context "classes" do
      setup do
        RESULT.clear

        (@class = Class.new).class_eval do
          class << self
            def foo
              RESULT << [:class, :foo]
              "class.foo"
            end
          end

          def self.bar
            RESULT << [:class, :bar]
            "class.bar"
          end

          def foo(arg)
            RESULT << [:class, :foo, arg]
            "class#foo(#{arg})"
          end

          def bar
            RESULT << [:class, :bar]
            "class#bar"
          end
        end

        (@subclass = Class.new(@class)).class_eval do
          def foo(arg)
            RESULT << [:subclass, :foo, arg, :before]
            super
            RESULT << [:subclass, :foo, arg, :after]
            "subclass#foo(#{arg})"
          end
        end
      end

      test "wrapping class instance methods" do
        @class.before(:foo) { RESULT << [:class, :before] }
        @class.after(:foo, :bar) { RESULT << [:class, :after] }

        instance = @class.new

        assert_equal "class#foo(qux)", instance.foo(:qux)
        assert_equal "class#bar", instance.bar

        assert_equal [
          [:class, :before],
          [:class, :foo, :qux],
          [:class, :after],
          [:class, :bar],
          [:class, :after]
        ], RESULT
      end

      test "wrapping class methods" do
        @class.eigenclass.before(:foo) { RESULT << [:class, :before] }
        @class.eigenclass.after(:foo, :bar) { RESULT << [:class, :after] }

        assert_equal "class.foo", @class.foo
        assert_equal "class.bar", @class.bar

        assert_equal [
          [:class, :before],
          [:class, :foo],
          [:class, :after],
          [:class, :bar],
          [:class, :after]
        ], RESULT
      end

      test "wrapping subclass instance methods" do
        @subclass.before(:foo) { RESULT << [:subclass, :before] }
        @subclass.after(:foo) { RESULT << [:subclass, :after] }

        assert_equal "subclass#foo(quux)", @subclass.new.foo(:quux)
        assert_equal [
          [:subclass, :before],
          [:subclass, :foo, :quux, :before],
          [:class, :foo, :quux],
          [:subclass, :foo, :quux, :after],
          [:subclass, :after]
        ], RESULT
      end

      test "wrapping instance methods" do
        instance = @class.new
        other = @class.new

        instance.eigenclass.before(:foo) { RESULT << [:instance, :before] }
        instance.eigenclass.after(:foo) { RESULT << [:instance, :after] }

        assert_equal "class#foo(instance)", instance.foo(:instance)
        assert_equal "class#foo(other)", other.foo(:other)

        assert_equal [
          [:instance, :before],
          [:class, :foo, :instance],
          [:instance, :after],
          [:class, :foo, :other]
        ], RESULT
      end
    end

    context "modules" do
      setup do
        RESULT.clear

        (@module = Module.new).module_eval do
          def bar
            RESULT << [:module, :bar]
            foo :bar
          end

          module_function

          def foo(arg = nil)
            RESULT << [:module, :foo, arg].compact
            "module.foo(#{arg})"
          end
        end

        @class = Class.new
        @class.include @module
      end

      test "wrapping module methods" do
        @module.before(:foo) { RESULT << [:module, :before] }
        @module.after(:foo, :bar) { RESULT << [:module, :after] }

        assert_equal "module.foo()", @module.foo

        assert_raises NoMethodError do
          @module.bar
        end

        instance = @class.new

        assert_equal "module.foo(bar)", instance.bar
        assert_raises NoMethodError do
          instance.foo
        end

        assert_equal [
          [:module, :before],
          [:module, :foo],
          [:module, :after],
          [:module, :bar],
          [:module, :before],
          [:module, :foo, :bar],
          [:module, :after],
          [:module, :after]
        ], RESULT
      end
    end

    context "main" do
      setup do
        RESULT.clear

        (@module = Module.new).module_eval do
          def foo(arg)
            RESULT << [:module, :foo, arg]
            "module.foo(#{arg})"
          end
        end
      end

      test "wrapping main methods" do
        (main = TOPLEVEL_BINDING.eval("self")).extend @module
        main.eigenclass.before(:foo) { RESULT << [:main, :before] }
        main.eigenclass.after(:foo) { RESULT << [:main, :after] }

        assert_equal "module.foo(qux)", main.foo(:qux)

        assert_equal [
          [:main, :before],
          [:module, :foo, :qux],
          [:main, :after]
        ], RESULT
      end
    end
  end
end
