# frozen_string_literal: true

require "minitest/assertions"

module Minitest
  module Assertions
    NL = '\n'
    START = '\A'
    FINISH = '\z'
    COL = ->(n) { "\\e\\[#{n}m" }
    BLANK = COL[0]
    RED = COL[31]
    YELLOW = COL[33]
    WHITE = COL[37]
    FILE = __FILE__.sub(/.*?(?=test)/, "")
    METHOD = "block in assert_shout"

    TITLE_LEFT = "#{RED}┏━#{BLANK} #{YELLOW}"
    TITLE_RIGHT = "#{BLANK} #{RED}━+┓#{BLANK}"
    TITLE = "#{START}#{NL}#{TITLE_LEFT}.*?#{TITLE_RIGHT}"

    BODY_LEFT = "#{RED}┃ #{BLANK} #{WHITE}"
    BODY_RIGHT = "#{BLANK} #{RED} +┃#{BLANK}"
    BODY = "#{BODY_LEFT} +.*? +#{BODY_RIGHT}"

    FOOTER_LEFT = "#{RED}┗━+#{BLANK} #{YELLOW}"
    FOOTER_RIGHT = "#{BLANK} #{RED}━┛#{BLANK}"
    FOOTER_CONTENT = "#{FILE} #{RED}━#{BLANK}#{YELLOW} " \
                     "\\d+ #{RED}━#{BLANK}#{YELLOW} #{METHOD}"
    FOOTER = "#{FOOTER_LEFT}#{FOOTER_CONTENT}#{FOOTER_RIGHT}#{NL}#{NL}#{FINISH}"

    def assert_shout(content, title: nil)
      assert_output shout_content(content, title) do
        yield.shout
      end
    end

    def shout_content(content, title = nil)
      /#{
        [
          prepare_title(title),
          content_line(nil),
          prepare_shout(content),
          content_line(nil),
          caller_locations(1, 1)[0].path.match(FILE) ? FOOTER : nil
        ].join(NL)
      }/m
    end

    private

    def prepare_title(content)
      return TITLE unless content

      if content.is_a? Regexp
        top_content_line content
      else
        top_content_line Regexp.quote(content)
      end
    end

    def prepare_shout(content)
      if content.is_a? Regexp
        content_line content
      else
        content.split("\n").map do |line|
          content_line Regexp.quote(line)
        end.join(NL)
      end
    end

    def top_content_line(str)
      [START, NL, TITLE_LEFT, str, TITLE_RIGHT].join
    end

    def content_line(str)
      [BODY_LEFT, str, BODY_RIGHT].join
    end
  end
end
