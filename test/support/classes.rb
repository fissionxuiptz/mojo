# frozen_string_literal: true

class Bar
  @@class_var = [] # rubocop:disable Style/ClassVars

  attr_reader :attr_reader

  def initialize(obj)
    @attr_reader = obj
    @@class_var << self
  end

  def bar_other_instance_method; end

  def bar_instance_method; end

  def self.class_var_reader
    @@class_var
  end

  def self.bar_class_method; end
end

class Foo < Bar
  def self.foo_class_method; end

  def foo_instance_method; end
end

class Bam < Foo
  def as_json(*)
    { attr_reader: @attr_reader.class }
  end
end

module Qux
  def qux_instance_method; end

  def self.qux_class_method; end
end

class QuxExtended < Foo
  extend Qux
end

class QuxIncluded < Foo
  include Qux
end

class Baz < DelegateClass(Bar)
  def delegatee
    __getobj__.class.inspect
  end
end
