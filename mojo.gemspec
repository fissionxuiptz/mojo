# frozen_string_literal: true

# Gemfury doesn't like __dir__
lib = File.expand_path "../lib", __FILE__ # rubocop:disable Style/ExpandPathArguments
$LOAD_PATH.unshift lib unless $LOAD_PATH.include? lib
require "mojo/version"

Gem::Specification.new do |s|
  s.name        = "mojo-fission"
  s.version     = Mojo::VERSION
  s.author      = "Fission Xuiptz"
  s.email       = "fissionxuiptz@softwaremojo.com"
  s.homepage    = "http://gitlab.com/fissionxuiptz/mojo"
  s.license     = "MIT"

  s.summary     = "SoftwareMojo library"
  s.description = "A library of useful ruby stuff"

  s.files         = `git ls-files`.split "\n"
  s.executables   = s.files.grep(%r{^bin/}) { |f| File.basename f }
  s.require_paths = ["lib"]

  s.required_ruby_version = Gem::Requirement.new(">= 2.7")

  s.add_runtime_dependency("activesupport", ">= 5", "<= 100")
  s.add_runtime_dependency("amazing_print", "~> 1.2")
  s.add_runtime_dependency("hirber", "~> 0.8")
  s.add_runtime_dependency("rainbow", "~> 3.0")
  s.add_runtime_dependency("unicode-display_width", ">= 1.8", "<= 100")

  s.metadata["rubygems_mfa_required"] = "true"
end
