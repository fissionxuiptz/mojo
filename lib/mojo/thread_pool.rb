# frozen_string_literal: true

module Mojo
  class ThreadPool
    def initialize(size)
      @size = size
      @jobs = Queue.new

      @pool = Array.new(@size) do |i|
        Thread.new do
          Thread.current[:id] = "#{__id__}:#{@size}:#{i}"

          catch(:exit) do
            loop do
              job, args = @jobs.pop
              job.call(*args)
            end
          end
        end
      end
    end

    def call(*args, &block)
      @jobs << [block, args]
    end

    def shutdown
      @size.times do
        call { throw :exit }
      end

      @pool.each(&:join)
    end
  end
end
