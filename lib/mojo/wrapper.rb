# frozen_string_literal: true

module Mojo
  module Wrapper
    SEMAPHORE = Mutex.new

    def wrap(*method_names, &block) # rubocop:disable Lint/UnusedMethodArgument
      raise ArgumentError, "method_name is missing" if method_names.empty?
      raise ArgumentError, "block is missing" unless block_given?

      SEMAPHORE.synchronize do
        method_names.flatten.each do |method_name|
          previous_method_name = [
            :_, :wrap, :previous, method_name,
            SecureRandom.public_send(
              (SecureRandom.respond_to?(:base36) ? :base36 : :base58), 8
            )
          ].join "_"

          module_eval <<~CODE, __FILE__, __LINE__ + 1
            method = instance_method(:#{method_name}) rescue nil                          # method = instance_method(:foobar) rescue nil

            define_method(:#{previous_method_name}) do                                    # define_method(:__wrap_previous_foobar_zhf8oj9w) do
              [method, block]                                                             #   [method, block]
            end                                                                           # end

            undef_method :__class rescue nil                                              # undef_method :__class rescue nil
            undef_method :__is_a? rescue nil                                              # undef_method :__is_a? rescue nil

            define_method :__class do                                                     # define_method :__class do
              Object.instance_method(:class).bind(self).call                              #   Object.instance_method(:class).bind(self).call
            end                                                                           # end

            define_method :__is_a? do |klass|                                             # define_method :__is_a? do |klass|
              Object.instance_method(:is_a?).bind(self).call klass                        #   Object.instance_method(:is_a?).bind(self).call klass
            end                                                                           # end

            unless :#{method_name} == :initialize                                         # unless :foobar == :initialize
              undef_method :#{method_name} rescue nil                                     #   undef_method :foobar rescue nil
            end                                                                           # end

            def #{method_name}(*args, &blk)                                               # def foobar(*args, &blk)
              context = if __is_a? Module                                                 #   context = if __is_a? Module
                          eigenclass                                                      #               eigenclass
                        elsif __class.respond_to? :#{previous_method_name}                #             elsif __class.respond_to? :__wrap_previous_foobar_zhf8oj9w
                          __class                                                         #               __class
                        else                                                              #             else
                          self                                                            #               self
                        end                                                               #             end

              previous = nil                                                              #   previous = nil

              if context.respond_to? :instance_method                                     #   if context.respond_to? :instance_method
                previous = context.instance_method(:#{previous_method_name})              #     previous = context.instance_method(:__wrap_previous_foobar_zhf8oj9w)

                begin                                                                     #     begin
                  previous = previous.bind(_self ||= self)                                #       previous = previous.bind(_self ||= self)
                rescue TypeError                                                          #     rescue TypeError
                  retry if _self = _self.superclass                                       #       retry if _self = _self.superclass
                end                                                                       #     end
              else                                                                        #   else
                previous = context.method(:#{previous_method_name})                       #     previous = context.method(:__wrap_previous_foobar_zhf8oj9w)
              end                                                                         #   end

              old, new = previous.call                                                    #   old, new = previous.call

              old ||= context.superclass.instance_method(:#{method_name}) rescue nil      #   old ||= context.superclass.instance_method(:foobar) rescue nil

              begin                                                                       #   begin
                old &&= old.bind(_self ||= self)                                          #     old &&= old.bind(_self ||= self)
              rescue TypeError                                                            #   rescue TypeError
                retry if _self = _self.superclass                                         #     retry if _self = _self.superclass
              end                                                                         #   end

              new.call old, args, blk, self                                               #   new.call old, args, blk, self
            end                                                                           # end

            return unless instance_of? Module                                             # return unless instance_of? Module

            # If our new method is already available publicly, the module has
            # extended itself and we're good to go
            return if singleton_methods.include? :#{previous_method_name}                 # return if singleton_methods.include? :__wrap_previous_foobar_zhf8oj9w

            # If the original method isn't already a singleton method,
            # it's not intended to be a module_function, so respect that
            return unless singleton_methods.include? :#{method_name}                      # return unless singleton_methods.include? :foobar

            class << self                                                                 # class << self
              # Hush the redefinition warning from using module_function
              undef_method :#{method_name}                                                #   undef_method :foobar
            end                                                                           # end

            module_function :#{method_name}, :#{previous_method_name}, :__class, :__is_a? # module_function :foobar, :__wrap_previous_foobar_zhf8oj9w, :__class, :__is_a?
          CODE
        end
      end
    end

    def before(*method_names, &block)
      around(*method_names, before: true, &block)
    end

    def after(*method_names, &block)
      around(*method_names, after: true, &block)
    end

    def around(*method_names, before: false, after: false)
      raise ArgumentError, "method_name is missing" if method_names.empty?
      raise ArgumentError, "block is missing" unless block_given?

      method_names.flatten.each do |method_name|
        wrap(method_name) do |original, args, block, obj|
          yield obj, method_name, args, block if before

          original&.call(*args, &block).tap_if(after) do # rubocop:disable Lint/SafeNavigationChain
            yield obj, method_name, args, block
          end
        end
      end
    end
  end
end

Module.include Mojo::Wrapper
