# frozen_string_literal: true

require "mojo/shouter/shout"

module Mojo
  module Shouter
    def out(*args, &block)
      Shout.out self, *args, &block
    end
    alias shout out
  end
end

Delegator.include Mojo::Shouter
Object.include    Mojo::Shouter
