# frozen_string_literal: true

require "hirber"
require "unicode/display_width"
require "unicode/display_width/string_ext" rescue nil

# Unicode support by miaout17
# [https://github.com/miaout17/hirb-unicode]
module Mojo
  module Console
    module Unicode
      def size(string)
        string.display_width
      end

      def slice(string, offset, width)
        chars = string.chars.to_a[offset..-1]

        current_length = 0
        split_index = 0
        chars.each_with_index do |c, i|
          char_width = size(c)
          break if current_length + char_width > width

          split_index = i + 1
          current_length += char_width
        end

        split_index ||= chars.count
        chars[0, split_index].join
      end

      def ljust(string, desired_width)
        string + (" " * (desired_width - size(string)))
      end

      def rjust(string, desired_width)
        (" " * (desired_width - size(string))) + string
      end
    end
  end
end

Hirb::String.extend Mojo::Console::Unicode
Hirb.enable pager: false
