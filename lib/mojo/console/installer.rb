# frozen_string_literal: true

module Mojo
  module Console
    module Installer
      def install(lib_name, gem_name = nil)
        gem_name ||= lib_name

        begin
          require lib_name
        rescue LoadError
          require "rubygems/dependency_installer"

          Gem::DependencyInstaller.new.install gem_name
          require lib_name rescue nil # Well we tried
        end
      end
    end
  end
end

top_level.extend Mojo::Console::Installer
