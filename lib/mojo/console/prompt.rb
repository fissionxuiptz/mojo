# frozen_string_literal: true

require "mojo/console/prompt/builder"

module Mojo
  module Console
    module Prompt
      module_function

      def pwd
        Dir.pwd.sub Dir.home, "~"
      end
    end
  end
end

%i[IRB Pry].each do |repl|
  next unless Object.const_defined? repl

  require "mojo/console/prompt/#{repl.to_s.underscore}"
  Mojo::Console::Prompt.apply!
  break
end
