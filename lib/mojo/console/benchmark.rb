# frozen_string_literal: true

require "benchmark"

module Mojo
  module Console
    module Benchmark
      def quick(reps = 100, &block)
        ::Benchmark.bmbm { |b| b.report { reps.times(&block) } }
        nil
      end

      def timer
        puts(::Benchmark.measure { yield })
      end
    end
  end
end

top_level.extend Mojo::Console::Benchmark
