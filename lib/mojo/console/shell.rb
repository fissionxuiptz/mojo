# frozen_string_literal: true

module Mojo
  module Console
    module Shell
      def paste
        IO.popen("pbpaste", "r").read
      end
    end
  end
end

top_level.extend Mojo::Console::Shell
