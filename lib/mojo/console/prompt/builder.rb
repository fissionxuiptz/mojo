# frozen_string_literal: true

require "rainbow/refinement"

using Rainbow

module Mojo
  module Console
    module Prompt
      class Builder
        def self.string(...)
          new(...).to_s
        end

        delegate :to_s, to: :@segments

        def initialize(*args, &block)
          params = args.extract_options!

          @context, @nesting, @pry_instance = args

          params.each do |ivar, value|
            instance_variable_set "@#{ivar}", value
            define_singleton_method(ivar) { instance_variable_get "@#{ivar}" }
          end

          instance_eval(&block) if block_given?
        end

        def segment(*args)
          Segment.new(*args).tap do |segment|
            @segments ? @segments << segment : @segments = segment
          end
        end

        def pry_context(*args)
          return unless @context and @nesting

          segment "#{Pry.view_clip(@context)}#{":#{@nesting}" if @nesting.positive?}", *args
        end

        def pry_input_counter(*args)
          return unless @pry_instance

          segment "pry:#{pry_input_count}", *args
        end

        def time(*args)
          segment Time.parse(`date`).strftime("%l:%M:%S%P"), *args
        end

        private

        def pry_input_count
          return unless @pry_instance

          method = @pry_instance.respond_to?(:input_ring) ? :input_ring : :input_array
          @pry_instance.__send__(method).count
        end

        class Segment
          SEPARATOR = ""

          attr_reader :content, :colour, :background

          def initialize(content = nil, colour = nil, background = nil)
            @content = colour ? content.to_s : " " * content.to_s.size
            @colour = colour
            @background = background
            @next = nil
          end

          def <<(segment)
            @next ? @next << segment : @next = segment
          end

          def to_s
            str =
              if content.size.positive?
                next_colour = @next ? @next.background || :black : nil
                { fg: colour, bg: background || :black }.inject(" #{content} ", &maybe_colour) +
                  { fg: background || :black, bg: next_colour }.inject(SEPARATOR, &maybe_colour)
              end

            "#{str}#{@next || ' '}"
          end

          private

          def maybe_colour
            ->(str, (type, colour)) {
              colour ? str.public_send(type, colour) : str
            }
          end
        end
      end
    end
  end
end
