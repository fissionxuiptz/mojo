# frozen_string_literal: true

require "rainbow/refinement"

using Rainbow

module Mojo
  module Console
    module Prompt
      module_function

      def apply!
        IRB.conf[:PROMPT][:SHORT] = {
          PROMPT_I: "%n #{Builder::Segment::SEPARATOR * 3} ",
          PROMPT_S: "%n%l ",
          PROMPT_C: "%n   * ",
          RETURN: "#{Builder::Segment::SEPARATOR.red} %s\n"
        }

        IRB.conf[:PROMPT][:INFORMATIVE] = {
          PROMPT_I: prompt,
          PROMPT_S: short_prompt,
          PROMPT_C: short_prompt,
          RETURN: "#{Builder::Segment::SEPARATOR.red} %s\n"
        }

        IRB.conf[:PROMPT_MODE] = :INFORMATIVE
        Mojo.top_level.try(:conf).try(:prompt_mode=, :INFORMATIVE)
      end

      def version
        "#{ruby_version} (mojo #{Mojo.version})"
      end

      def prompt
        Builder.string(version: version, pwd: pwd) do
          segment version,  :white,  :red
          segment pwd,      :yellow, :black
          segment "%m",     :white,  :red
          time              :yellow, :black
          segment "irb:%n", :white,  :red
        end
      end

      def short_prompt
        Builder.string(version: version, pwd: pwd) do
          segment version
          segment pwd
          segment "%m", :black, :black
          time
          segment "    *", :white, :blue
        end
      end
    end
  end
end

AmazingPrint.irb!
