# frozen_string_literal: true

module Mojo
  module Console
    module Prompt
      module_function

      def apply!
        version = "#{ruby_version} (mojo #{Mojo.version})"

        prompts = [
          proc do |*args|
            Builder.string(*args, version: version, pwd: pwd) do
              segment version,  :white,  :red
              segment pwd,      :yellow, :black
              pry_context       :white,  :red
              time              :yellow, :black
              pry_input_counter :white,  :red
            end
          end,
          proc do |*args|
            Builder.string(*args, version: version, pwd: pwd) do
              segment version
              segment pwd
              pry_context
              time

              indent = " " * (pry_input_count.to_s.size + 3)
              segment "#{indent}*", :white, :blue
            end
          end
        ]

        Pry.config.prompt =
          if defined? Pry::Prompt
            Pry::Prompt.new("mojo", "mojo prompt", prompts)
          else
            prompts
          end
      end
    end
  end
end

AmazingPrint.pry!
