# frozen_string_literal: true

require "logger"

module Mojo
  module Console
    module Rails
      def sql(str = "")
        res = ::ActiveRecord::Base.connection.execute(str)
        res.entries.any? ? res.entries : res
      end

      def __loggers
        @__loggers ||= {
          rails: ::Rails.logger.clone,
          ar_base: ::ActiveRecord::Base.logger.clone,
          stdout: ActiveSupport::TaggedLogging.new(Logger.new($stdout))
        }
      end

      def log_to_stdout
        ::Rails.logger = __loggers[:stdout]
        ::ActiveRecord::Base.logger = __loggers[:stdout]
      end

      def original_logging
        ::Rails.logger = __loggers[:rails]
        ::ActiveRecord::Base.logger = __loggers[:ar_base]
      end

      def logging_off
        ::Rails.logger = ::ActiveRecord::Base.logger = nil
      end

      module ActiveRecord
        def truncate
          connection.exec_query "TRUNCATE #{table_name} RESTART IDENTITY CASCADE"
        end
      end
    end
  end
end

if defined? Rails
  top_level.extend Mojo::Console::Rails
  ActiveRecord::Base.extend Mojo::Console::Rails::ActiveRecord
end
