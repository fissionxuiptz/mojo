# frozen_string_literal: true

module Mojo
  module CoreExt
    module NilClass
      def try(*args, &block)
        return if args.empty?

        public_send(*args, &block) if respond_to? args.first
      end
    end
  end
end

NilClass.prepend Mojo::CoreExt::NilClass
