# frozen_string_literal: true

module Kernel
  delegate :ruby_version, :top_level, to: :Mojo
end
