# frozen_string_literal: true

module Mojo
  module CoreExt
    module Object
      def eigenclass
        class << self
          self
        end
      end

      def interesting_methods
        if instance_of? ::Module
          (singleton_methods + instance_methods).uniq.sort
        else
          (public_methods - ::Object.methods - ::Module.methods).sort
        end
      end
      alias im interesting_methods

      def tap_if(*args)
        if args.empty?
          yield self if self
          return self
        end

        case args[0]
        when Proc   then yield self if instance_exec(&args[0])
        when Symbol then yield self if try(*args)
        else             yield self if args[0]
        end

        self
      end

      def tap_unless(*args)
        if args.empty?
          yield self unless self
          return self
        end

        case args[0]
        when Proc       then yield self unless instance_exec(&args[0])
        when Symbol     then yield self unless try(*args)
        when nil, false then yield self
        end

        self
      end
    end
  end
end

Object.prepend Mojo::CoreExt::Object
NilClass.prepend Mojo::CoreExt::Object
