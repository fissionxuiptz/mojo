# frozen_string_literal: true

module Mojo
  class DevNull < BasicObject
    def new
      self
    end

    private

    def respond_to_missing?(*)
      true
    end

    def method_missing(*)
      self
    end
  end
end
