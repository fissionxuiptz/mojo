# frozen_string_literal: true

module Mojo
  module Shouter
    module DelegateClass
      def DelegateClass(superclass) # rubocop:disable Naming/MethodName
        Class.new(Delegator).tap do |klass|
          methods = superclass.instance_methods
          methods -= ::Delegator.public_api
          methods -= [:to_s, :inspect, :=~, :!~, :===]
          methods -= [:shout, :out]

          klass.module_eval do
            mattr_reader :delegate_class, default: superclass

            def __getobj__
              unless defined?(@delegate_dc_obj)
                return yield if block_given?

                __raise__ ::ArgumentError, "not delegated"
              end

              @delegate_dc_obj
            end

            def __setobj__(obj)
              __raise__ ::ArgumentError, "cannot delegate to self" if equal? obj
              @delegate_dc_obj = obj
            end

            methods.each do |method|
              define_method method, Delegator.delegating_block(method)
            end
          end

          klass.define_singleton_method :public_instance_methods do |all = true|
            super(all) - superclass.protected_instance_methods
          end

          klass.define_singleton_method :protected_instance_methods do |all = true|
            super(all) | superclass.protected_instance_methods
          end
        end
      end
    end
  end
end

top_level.extend Mojo::Shouter::DelegateClass
