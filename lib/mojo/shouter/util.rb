# frozen_string_literal: true

module Mojo
  module Shouter
    module Util
      SIMPLE_DATA_TYPES = [
        NilClass, FalseClass, TrueClass, Numeric, String, Symbol, Module
      ].freeze

      module_function

      def simple_data_types?(*objs)
        objs.flatten.all? do |obj|
          obj.blank? or SIMPLE_DATA_TYPES.any? do |klass|
            obj = obj.__getobj__ if obj.respond_to? :__getobj__
            obj.is_a? klass
          end
        end
      end
    end
  end
end
