# frozen_string_literal: true

require "rainbow/refinement"
require "mojo/shouter/delegate_class"
require "mojo/shouter/util"

Dir.glob(File.expand_path("types/*.rb", __dir__)).sort.each do |path|
  require path
end

using Rainbow
using Mojo::Shouter::Types

module Mojo
  module Shouter
    class Shout
      BOX = <<~SHAPE.split("\n").freeze
        ┏━┓
        ┃ ┃
        ┗━┛
      SHAPE

      class << self
        def out(obj, *args, &block)
          new(obj, *args, &block).out
        end

        def content(obj, *args, &block)
          new(obj, *args, &block).content
        end
      end

      attr_reader :obj, :location

      def initialize(obj, *args)
        @location = caller_locations[3]
        path = location.path.sub(%r{#{Dir.pwd}/}, "").sub(Dir.home, "~")
        label = "#{'#' if location.label =~ /\A[A-Za-z0-9_]+\z/}#{location.label}"

        @obj = obj
        @args = args

        if block_given?
          @raw_title = render_class(yielded_obj = yield(obj))
          @raw_body = yielded_obj.__shout
        else
          @raw_title = render_class(obj)
          @raw_body = obj.__shout
        end

        @raw_title = [@raw_title, *args]
        @raw_footer = [path, location.lineno, label]
      end

      def content
        {
          title: @raw_title,
          body: @raw_body,
          footer: @raw_footer
        }
      end

      def title
        @title ||= @raw_title.join(" #{BOX[0][1]} ")
      end

      def body
        @body ||= @raw_body
      end

      def footer
        @footer ||= @raw_footer.join(" #{BOX[2][1]} ")
      end

      def out
        @limit = window_width - 6
        @width = max_str_width

        puts

        [title, body, footer].each_with_index do |str, i|
          puts format if i == 1
          str.split("\n").each { |s| puts format(s, i) }
          puts format if i == 1
        end

        puts

        location.path == "(pry)" ? DevNull : obj
      end

      private

      def render_class(obj)
        superclass =
          if obj.respond_to? :__getobj__
            "DelegateClass(#{obj.__getobj__.class})"
          else
            obj.class.superclass
          end

        "#{obj.class} < #{superclass}"
      end

      def format(str = nil, pos = 1)
        if pos == 1 && (str&.length || 0) > @limit
          return str.scan(/.{1,#{@limit}}/).map { |s| format s }
        end

        parts = [recolour(str.to_s.__send__(pos == 1 ? :white : :yellow), BOX[pos][1], :red)]

        if pos == 2
          parts.unshift (BOX[pos][0] + (BOX[pos][1] * repeat(str))).red
          parts.push (BOX[pos][1..-1]).red
        else
          parts.unshift (BOX[pos][0..1]).red
          parts.push ((BOX[pos][1] * repeat(str)) + BOX[pos][2]).red
        end

        parts.join(" ")
      end

      def recolour(str, search, colour)
        return str if search == " "

        old_colour = str.scan(/\e\[[0-9;]*m/).first
        str.gsub(search, search.__send__(colour) + old_colour.to_s)
      end

      def repeat(str)
        @width - [str&.length || 0, @limit].min
      end

      def max_str_width
        [
          [title, *body.split("\n"), footer].max do |a, b|
            a.length <=> b.length
          end.length,
          @limit
        ].min + 1
      end

      def window_width
        fd = IO.sysopen(`tty`.chomp, "w")
        IO.new(fd, "w").ioctl(OS::TIOCGWINSZ, window = [0, 0].pack("SS"))
        window.unpack("SS").last
      end
    end
  end
end
