# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine FalseClass do
        def __shout(*)
          inspect
        end
      end
    end
  end
end
