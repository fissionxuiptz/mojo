# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine Symbol do
        def __shout(_depth = 0, key: false)
          key ? as_json : inspect
        end
      end
    end
  end
end
