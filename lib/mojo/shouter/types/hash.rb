# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine Hash do
        def single_line?(depth)
          depth < 2 and
            keys.count < 3 and
            Util.simple_data_types? keys, values
        end

        def __shout(depth = 0, *)
          nl = single_line?(depth) ? "" : "\n"
          indent, ws = nl.empty? ? ["", " "] : ["  ", ""]
          sep = " "
          depth += 1
          first = true
          result = "{#{ws if keys.present?}#{nl}"

          each do |key, val|
            result += ",#{ws}#{nl}" unless first
            result += indent * depth
            result += key.__shout(depth, key: true)
            result += (key.is_a?(Symbol) ? ":#{sep}" : "#{sep}=>#{sep}")
            result += val.__shout(depth)
            first = false
          end

          depth -= 1
          result += "#{nl}#{indent * depth}#{ws if keys.present?}}"
        end
      end
    end
  end
end
