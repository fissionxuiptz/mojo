# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine String do
        def __shout(*)
          inspect
        end
      end
    end
  end
end
