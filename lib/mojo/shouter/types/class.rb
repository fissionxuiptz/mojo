# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine Class do
        def __shout(_depth = 0, key: false)
          superclass =
            if respond_to? :delegate_class
              "DelegateClass(#{delegate_class})"
            else
              self.superclass
            end

          [self, key ? nil : superclass].compact.join " < "
        end
      end
    end
  end
end
