# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine Module do
        def __shout(*)
          to_s
        end
      end
    end
  end
end
