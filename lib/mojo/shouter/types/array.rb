# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine Array do
        def __shout(depth = 0, *)
          nl = Util.simple_data_types?(self) ? "" : "\n"
          indent, ws = nl.empty? ? ["", " "] : ["  ", ""]
          depth += 1
          first = true
          result = "[#{nl}"

          each do |obj|
            result += ",#{ws}#{nl}" unless first
            result += indent * depth
            result += obj.__shout(depth)
            first = false
          end

          depth -= 1
          result += "#{nl}#{indent * depth}]"
        end
      end
    end
  end
end
