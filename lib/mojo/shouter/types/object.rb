# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine Object do
        def __shout(depth = 0, *)
          as_json.__shout depth
        end

        def as_json(options = nil)
          super
        rescue SystemStackError
          instance_values
        end
      end
    end
  end
end
