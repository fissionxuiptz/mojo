# frozen_string_literal: true

module Mojo
  module Shouter
    module Types
      refine BigDecimal do
        def __shout(*)
          finite? ? as_json : to_s
        end
      end
    end
  end
end
