# frozen_string_literal: true

require "amazing_print"
require "mojo"

Dir["#{__dir__}/console/*.rb"].sort.each do |file|
  require "mojo/console/#{File.basename(file, '.rb')}"
end

top_level.extend Hirb::Console               if defined? Hirb::Console
top_level.extend FactoryBot::Syntax::Methods if defined? FactoryBot::Syntax::Methods
