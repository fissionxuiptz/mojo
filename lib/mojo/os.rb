# frozen_string_literal: true

module Mojo
  module OS
    class << self
      def windows?
        defined? RUBY_PLATFORM and RUBY_PLATFORM =~ /cygwin|mswin|mingw|bccwin|wince|emx/
      end

      def mac?
        defined? RUBY_PLATFORM and RUBY_PLATFORM =~ /darwin/
      end

      def unix?
        !windows?
      end

      def linux?
        unix? and not mac?
      end
    end

    TIOCGWINSZ = if    mac?     then 0x40087468
                 elsif windows? then 0x5401
                 else                0x5413
                 end
  end
end
