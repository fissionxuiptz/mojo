# encoding: utf-8
# frozen_string_literal: true

module Mojo
  VERSION = "0.8.7"

  module_function

  def version
    VERSION
  end

  def ruby_version
    [(RUBY_ENGINE == "ruby" ? " " : RUBY_ENGINE), RUBY_ENGINE_VERSION].compact.join(" ")
  end
end
