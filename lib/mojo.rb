# frozen_string_literal: true

require "active_support"
require "active_support/core_ext"

require "mojo/version"

module Mojo
  module_function def top_level
    TOPLEVEL_BINDING.eval "self"
  end
end

require "mojo/core_ext/kernel"
require "mojo/core_ext/nil_class"
require "mojo/core_ext/object"
require "mojo/dev_null"
require "mojo/shouter"
require "mojo/thread_pool"
require "mojo/wrapper"
